package boutiqueklf.controller.ventas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import boutiqueklf.model.ventas.dtos.DTODetalleVenta;
import boutiqueklf.model.ventas.managers.ManagerVenta;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import boutiqueklf.controller.JSFUtil;
import boutiqueklf.model.core.entities.CliCliente;
import boutiqueklf.model.core.entities.InvProducto;
import boutiqueklf.model.core.entities.ThmEmpleado;
import boutiqueklf.model.core.entities.VenFacturaCabecera;
import boutiqueklf.model.core.entities.VenFacturaDetalle;

@Named
@SessionScoped
public class BeanVentas implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerVenta managerVenta;

	private List<InvProducto> listaProductos;
	private List<CliCliente> listaClientes;
	private List<ThmEmpleado> listaEmpleados;
	private List<DTODetalleVenta> listaDetalleVenta;
	private List<VenFacturaCabecera> listaFacturaCabecera;
	private List<VenFacturaDetalle> listaFacturaDetalle;

	private String cedulaClienteSeleccionado;
	private int idEmpleadoSeleccionado;
	private int idProductoSeleccionado;
	private double total;
	private int cantidad;
	private double descuento;
	private double iva;
	private double totalapagar;
	private VenFacturaCabecera cabecera;
	private VenFacturaDetalle detalle;	
	private String fecha;
	private int idfaccab;
	
	

	@PostConstruct
	public void inicializar() {
		listaProductos = managerVenta.findAllInvProducto();
		listaClientes = managerVenta.findAllCliCliente();
		listaEmpleados = managerVenta.findAllThmEmpleado();
		listaFacturaCabecera = managerVenta.findAllVenFacturaCAbecera();
		listaDetalleVenta = new ArrayList<DTODetalleVenta>();
	}

	// Detalle ventas
	public void actionListenerAgregarDetalleVenta() {
		listaDetalleVenta.add(managerVenta.crearDetalleVenta(idProductoSeleccionado, cantidad));
		total = managerVenta.subtotalDetalle(listaDetalleVenta);
		descuento=managerVenta.descuento(listaDetalleVenta);
		iva=managerVenta.iva(listaDetalleVenta);
		totalapagar=managerVenta.totalApagar(listaDetalleVenta);
	}

	public String actionListenerGuardarVentaMultiple() {		
		
		try {
			managerVenta.ventaMultiple(listaDetalleVenta, cedulaClienteSeleccionado, idEmpleadoSeleccionado);		
			JSFUtil.crearMensajeINFO("Factura Generada");
			listaDetalleVenta = new ArrayList<DTODetalleVenta>();
			total= 0;
			descuento=0;
			iva=0;
			totalapagar=0;
			
			return" ";			
			
		}catch (Exception e) {
			FacesMessage mensaje=new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
			FacesContext.getCurrentInstance().addMessage(null, mensaje);
			e.printStackTrace();
		}
		return "";
		
	}
	// eliminar producto
	public void actionListenerElimnarProducto(DTODetalleVenta p) {
		listaDetalleVenta=managerVenta.eliminarProducto(listaDetalleVenta, p.getIdProducto());
		JSFUtil.crearMensajeINFO("Producto ELiminado");
		total=managerVenta.subtotalDetalle(listaDetalleVenta);
		descuento=managerVenta.descuento(listaDetalleVenta);
		iva=managerVenta.iva(listaDetalleVenta);
		totalapagar=managerVenta.totalApagar(listaDetalleVenta);
	}
	
	//buscar por fecha;
	public void buscarRegistroVentaByFecha(){
		try {
			cabecera=managerVenta.findVentaByFecha(fecha);
			cedulaClienteSeleccionado = cabecera.getCliCliente().getCedulaCli();
			idfaccab = cabecera.getIdVenFacturaCabecera();
			idEmpleadoSeleccionado=cabecera.getThmEmpleado().getIdThmEmpleado();
			fecha=cabecera.getFecha().toString();
			total=cabecera.getSubtotal().doubleValue();
			descuento=cabecera.getDescuento().doubleValue();
			iva=cabecera.getIva().doubleValue();
			totalapagar=cabecera.getTotal().doubleValue();
			JSFUtil.crearMensajeINFO("Búsqueda encontrada");
		} catch (Exception e) {
			JSFUtil.crearMensajeINFO("Datos no encontrados");
			e.printStackTrace();
		}
	}
	
	public String actionReporte(){   
    	Map<String,Object> parametros=new HashMap<String,Object>();
    	/*parametros.put("p_titulo_principal",p_titulo_principal); 
    	 * parametros.put("p_titulo",p_titulo);*/                
    	FacesContext context=FacesContext.getCurrentInstance(); 
    	ServletContext servletContext=(ServletContext)context.getExternalContext().getContext(); 
    	String ruta=servletContext.getRealPath("ventas/facturas_reporte.jasper"); 
    	System.out.println(ruta); 
    	HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse(); 
    	response.addHeader("Content-disposition","attachment;filename=factutasreportes.pdf"); 
    	response.setContentType("application/pdf");
    	try{ 
    		Class.forName("org.postgresql.Driver");  
    		Connection connection =null;     
    		connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/wgsanchezq","wgsanchezq","1004043996"); 
    		JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);  
    		JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());  
    		context.getApplication().getStateManager().saveView( context );   
    		System.out.println("reporte generado.");  
    		context.responseComplete();
    		
    	}catch(Exception e){
    		JSFUtil.crearMensajeERROR(e.getMessage()); 
    		e.printStackTrace();
    		}
    	return"";
    	
    	}

	public String actionReporte2(){   
    	Map<String,Object> parametros=new HashMap<String,Object>();
    	/*parametros.put("p_titulo_principal",p_titulo_principal); 
    	 * parametros.put("p_titulo",p_titulo);*/                
    	FacesContext context=FacesContext.getCurrentInstance(); 
    	ServletContext servletContext=(ServletContext)context.getExternalContext().getContext(); 
    	String ruta=servletContext.getRealPath("ventas/factura.jasper"); 
    	System.out.println(ruta); 
    	HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse(); 
    	response.addHeader("Content-disposition","attachment;filename=factutas.pdf"); 
    	response.setContentType("application/pdf");
    	try{ 
    		Class.forName("org.postgresql.Driver");  
    		Connection connection =null;     
    		connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/wgsanchezq","wgsanchezq","1004043996"); 
    		JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);  
    		JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());  
    		context.getApplication().getStateManager().saveView( context );   
    		System.out.println("reporte generado.");  
    		context.responseComplete();
    		
    	}catch(Exception e){
    		JSFUtil.crearMensajeERROR(e.getMessage()); 
    		e.printStackTrace();
    		}
    	return"";
    	
    	}
    
	
	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<CliCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<CliCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public List<DTODetalleVenta> getListaDetalleVenta() {
		return listaDetalleVenta;
	}

	public void setListaDetalleVenta(List<DTODetalleVenta> listaDetalleVenta) {
		this.listaDetalleVenta = listaDetalleVenta;
	}

	public String getCedulaClienteSeleccionado() {
		return cedulaClienteSeleccionado;
	}

	public void setCedulaClienteSeleccionado(String cedulaClienteSeleccionado) {
		this.cedulaClienteSeleccionado = cedulaClienteSeleccionado;
	}

	public int getIdEmpleadoSeleccionado() {
		return idEmpleadoSeleccionado;
	}

	public void setIdEmpleadoSeleccionado(int idEmpleadoSeleccionado) {
		this.idEmpleadoSeleccionado = idEmpleadoSeleccionado;
	}

	public int getIdProductoSeleccionado() {
		return idProductoSeleccionado;
	}

	public void setIdProductoSeleccionado(int idProductoSeleccionado) {
		this.idProductoSeleccionado = idProductoSeleccionado;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public List<ThmEmpleado> getListaEmpleados() {
		return listaEmpleados;
	}

	public void setListaEmpleados(List<ThmEmpleado> listaEmpleados) {
		this.listaEmpleados = listaEmpleados;
	}

	public List<VenFacturaCabecera> getListaFacturaCabecera() {
		return listaFacturaCabecera;
	}

	public void setListaFacturaCabecera(List<VenFacturaCabecera> listaFacturaCabecera) {
		this.listaFacturaCabecera = listaFacturaCabecera;
	}

	public List<VenFacturaDetalle> getListaFacturaDetalle() {
		return listaFacturaDetalle;
	}

	public void setListaFacturaDetalle(List<VenFacturaDetalle> listaFacturaDetalle) {
		this.listaFacturaDetalle = listaFacturaDetalle;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getTotalapagar() {
		return totalapagar;
	}

	public void setTotalapagar(double totalapagar) {
		this.totalapagar = totalapagar;
	}

	public VenFacturaCabecera getCabecera() {
		return cabecera;
	}

	public void setCabecera(VenFacturaCabecera cabecera) {
		this.cabecera = cabecera;
	}

	public VenFacturaDetalle getDetalle() {
		return detalle;
	}

	public void setDetalle(VenFacturaDetalle detalle) {
		this.detalle = detalle;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getIdfaccab() {
		return idfaccab;
	}

	public void setIdfaccab(int idfaccab) {
		this.idfaccab = idfaccab;
	}
	
	

}
