package boutiqueklf.controller.clientes;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import boutiqueklf.controller.JSFUtil;
import boutiqueklf.model.cliente.managers.ManagerClientes;

import boutiqueklf.model.core.entities.CliCliente;
import boutiqueklf.model.core.entities.SegModulo;
import boutiqueklf.model.core.entities.SegUsuario;
import boutiqueklf.model.core.entities.ThmCargo;
import boutiqueklf.model.core.entities.ThmEmpleado;
import boutiqueklf.model.seguridades.managers.ManagerSeguridades;
import boutiqueklf.model.thumano.managers.ManagerTHumano;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanCliCliente implements Serializable {

	/**
	 * 
	 */
	
private static final long serialVersionUID = 1L;
	
	@EJB
	private ManagerClientes mClientes;
	private List<CliCliente> listaClientes;
	private CliCliente nuevoCliente;
	private String seleccion_cedula;
	private CliCliente actualizarCliente;
	
	
	@PostConstruct
	public void inicializar() {
		nuevoCliente=new CliCliente();
		//listaClientes=mClientes.findAllCliCliente();
	//	listaClientes=new ArrayList<CliCliente>();
		
	}

	public BeanCliCliente() {
	}

	public String actionCargarMenuClientes() {
		listaClientes=mClientes.findAllCliClientesv5();
		return "clientes?faces-redirect=true";
	}

	public String actionCargarMenuClientesActualizar() {
		listaClientes=mClientes.findAllCliClientesv5();
		return "clienteActualizar?faces-redirect=true";
	}

	public String actionMenuNuevoClientes() {
		listaClientes=mClientes.findAllCliClientesv5();
		nuevoCliente=new CliCliente();
		return "cliente_crear?faces-redirect=true";
	}
	public String actionCargarMenuClientesEliminar() {
		listaClientes=mClientes.findAllCliClientesv5();
		return "clienteEliminar?faces-redirect=true";
	}
	
	
	public String actionReporte(){
		Map<String,Object> parametros=new HashMap<String,Object>();
		/*parametros.put("p_titulo_principal",p_titulo_principal);
		parametros.put("p_titulo",p_titulo);*/
		FacesContext context=FacesContext.getCurrentInstance();
		ServletContext servletContext=(ServletContext)context.getExternalContext().getContext();
		String ruta=servletContext.getRealPath("cliente/reportCliente.jasper");
		System.out.println(ruta);
		HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte.pdf");
		response.setContentType("application/pdf");
		try {
		Class.forName("org.postgresql.Driver");
		Connection connection = null;
		connection =  DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/wgsanchezq","wgsanchezq", "1004043996");
		JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);
		JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
		context.getApplication().getStateManager().saveView ( context ) ;
		System.out.println("reporte generado.");
		context.responseComplete();
		} catch (Exception e) {
		JSFUtil.crearMensajeERROR(e.getMessage());
		e.printStackTrace();
		}
		return "";
		}
	
	
	
	//seleccionar--Cliente
	public void actionListenerSeleccionarClienteEdicion(CliCliente c) {
		actualizarCliente=c;
		seleccion_cedula=c.getCedulaCli();
	}
	
	//cargar
	public void actionListenerCargarClientes(CliCliente m) {
		actualizarCliente=m;
		}
	
//insertar--cliente

	public void actionListenerInsertarNuevoCLientev5() {
		try {
			mClientes.insertarClientev5(nuevoCliente);
			listaClientes=mClientes.findAllCliClientesv5();
			JSFUtil.crearMensajeINFO("Cliente creado.");
			nuevoCliente=new CliCliente();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	

	//guardar-ediciion
		public void actionListenerGuardarActualizacionClientev5() {
			try {
				mClientes.actualizarClientev5(actualizarCliente);
				JSFUtil.crearMensajeINFO("Cliente editado.");
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();
			}
			
		}
		//eliminar
		public void actionListenerEliminarClientev5(String cedula) {
			try {
				mClientes.eliminarClientev5(cedula);
				listaClientes=mClientes.findAllCliClientesv5();
				JSFUtil.crearMensajeINFO("Cliente eliminada");
			} catch (Exception e) {
				JSFUtil.crearMensajeERROR(e.getMessage());
				e.printStackTrace();
			}
		}
		public List<CliCliente> getListaClientes() {
			return listaClientes;
		}

		public void setListaClientes(List<CliCliente> listaClientes) {
			this.listaClientes = listaClientes;
		}

		public CliCliente getNuevoCliente() {
			return nuevoCliente;
		}

		public void setNuevoCliente(CliCliente nuevoCliente) {
			this.nuevoCliente = nuevoCliente;
		}

		public String getSeleccion_cedula() {
			return seleccion_cedula;
		}

		public void setSeleccion_cedula(String seleccion_cedula) {
			this.seleccion_cedula = seleccion_cedula;
		}

		public CliCliente getActualizarCliente() {
			return actualizarCliente;
		}

		public void setActualizarCliente(CliCliente actualizarCliente) {
			this.actualizarCliente = actualizarCliente;
		}

		public ManagerClientes getmClientes() {
			return mClientes;
		}

		public void setmClientes(ManagerClientes mClientes) {
			this.mClientes = mClientes;
		}
	
	
	
	
	
	
	
	
	

}
