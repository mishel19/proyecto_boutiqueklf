package boutiqueklf.controller.inventario;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import boutiqueklf.controller.JSFUtil;
import boutiqueklf.model.core.entities.CompProveedorDetalle;
import boutiqueklf.model.core.entities.InvProducto;
import boutiqueklf.model.core.entities.InvTalla;
import boutiqueklf.model.core.entities.ThmEmpleado;
import boutiqueklf.model.inventario.managers.ManagerInvProducto;
import boutiqueklf.model.ventas.managers.ManagerVenta;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanInvProducto implements Serializable {
	private static final long serialVersionUID = 1L;
    @EJB
    private ManagerInvProducto managerInvProducto;

    private List<InvProducto> listaProductos;

    private List<CompProveedorDetalle> listaProveedorDetalle;
    private List<InvTalla> listaTalla;

    private int id_prod;
    private int id_inv_talla;
    private String nombre_producto;
    private String descripcion;
    private double precio;
    private double cantidad_total;

    private int id_inv_tallaEditar;
    private double cantidad_totalIngresar;

    private InvProducto invProductoEditar;

    private InvProducto invProductoIngresar;

    @PostConstruct
    public void inicializarInvProducto() {
        listaProductos = managerInvProducto.findAllInvProducto();
        listaProveedorDetalle = managerInvProducto.findAllCompProveedorDetalle();
        listaTalla = managerInvProducto.findAllInvTalla();
    }

    public void actionlistenerInsertar() {
        //try {
        managerInvProducto.crearInvProducto(id_inv_talla, nombre_producto, descripcion, precio, cantidad_total);
        inicializarInvProducto();
        JSFUtil.crearMensajeINFO("Se ha insertado correctamente");
        //} catch (Exception e) {
        // TODO: handle exception
        //JSFUtil.crearMensajeError("Error al insertar" + e.getMessage());
        //}

    }

    public void actionlistenerActualizar() {
            System.out.println("Hola "+id_inv_tallaEditar);
            //if (id_inv_tallaEditar == 0) {
                JSFUtil.crearMensajeWARN("Ingrese una talla!");
            //} else {
                managerInvProducto.actualizarInvProducto(invProductoEditar, id_inv_tallaEditar);
                System.out.println("hh " + invProductoEditar.getInvTalla().getIdInvTalla());
                inicializarInvProducto();
                JSFUtil.crearMensajeINFO("Se ha actualizado");
            //}
        
    }

    public void actionSeleccionarProductoEditar(int id_prod) {
        invProductoEditar = managerInvProducto.findProductoByid(id_prod);
        inicializarInvProducto();
    }

    public void actionSeleccionarProductoIngresar(int id_prod) {
        invProductoIngresar = managerInvProducto.findProductoByid(id_prod);
        inicializarInvProducto();
    }

    public void actionlistenerIngresar() {
        try {
            managerInvProducto.ingresarInvProducto(invProductoIngresar, cantidad_totalIngresar);
            inicializarInvProducto();
            JSFUtil.crearMensajeINFO("Se ha ingresado: " + (cantidad_totalIngresar) + " " + invProductoIngresar.getNombreProducto());
        } catch (Exception e) {
            // TODO: handle exception
            JSFUtil.crearMensajeERROR("Error al ingresar Producto!" + e.getMessage());
        }
    }

    public void actionlistenerEliminarInvProducto(int id_prod) {
        try {
            if (managerInvProducto.eliminarInvProducto(id_prod)) {
                inicializarInvProducto();
                JSFUtil.crearMensajeINFO("Se ha eliminado");
            } else {
                JSFUtil.crearMensajeERROR("No se ha eliminado");

            }
        } catch (Exception e) {
            // TODO: handle exception
            JSFUtil.crearMensajeERROR(e.getMessage());
        }
    }
    
    
    //reporte
    public String actionReporte(){   
    	Map<String,Object> parametros=new HashMap<String,Object>();
    	/*parametros.put("p_titulo_principal",p_titulo_principal); 
    	 * parametros.put("p_titulo",p_titulo);*/                
    	FacesContext context=FacesContext.getCurrentInstance(); 
    	ServletContext servletContext=(ServletContext)context.getExternalContext().getContext(); 
    	String ruta=servletContext.getRealPath("inventario/reportes.jasper"); 
    	System.out.println(ruta); 
    	HttpServletResponse response=(HttpServletResponse)context.getExternalContext().getResponse(); 
    	response.addHeader("Content-disposition","attachment;filename=reportes.pdf"); 
    	response.setContentType("application/pdf");
    	try{ 
    		Class.forName("org.postgresql.Driver");  
    		Connection connection =null;     
    		connection = DriverManager.getConnection("jdbc:postgresql://192.100.198.141:5432/wgsanchezq","wgsanchezq","1004043996"); 
    		JasperPrint impresion=JasperFillManager.fillReport(ruta, parametros,connection);  
    		JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());  
    		context.getApplication().getStateManager().saveView( context );   
    		System.out.println("reporte generado.");  
    		context.responseComplete();
    		
    	}catch(Exception e){
    		JSFUtil.crearMensajeERROR(e.getMessage()); 
    		e.printStackTrace();
    		}
    	return"";
    	
    	}
    
    
        

    public List<InvProducto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<InvProducto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public List<CompProveedorDetalle> getListaProveedorDetalle() {
        return listaProveedorDetalle;
    }

    public void setListaProveedorDetalle(List<CompProveedorDetalle> listaProveedorDetalle) {
        this.listaProveedorDetalle = listaProveedorDetalle;
    }

    public List<InvTalla> getListaTalla() {
        return listaTalla;
    }

    public void setListaTalla(List<InvTalla> listaTalla) {
        this.listaTalla = listaTalla;
    }

    public int getId_prod() {
        return id_prod;
    }

    public void setId_prod(int id_prod) {
        this.id_prod = id_prod;
    }

    public int getId_inv_talla() {
        return id_inv_talla;
    }

    public void setId_inv_talla(int id_inv_talla) {
        this.id_inv_talla = id_inv_talla;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getCantidad_total() {
        return cantidad_total;
    }

    public void setCantidad_total(double cantidad_total) {
        this.cantidad_total = cantidad_total;
    }

    public InvProducto getInvProductoEditar() {
        return invProductoEditar;
    }

    public void setInvProductoEditar(InvProducto invProductoEditar) {
        this.invProductoEditar = invProductoEditar;
    }

    public InvProducto getInvProductoIngresar() {
        return invProductoIngresar;
    }

    public void setInvProductoIngresar(InvProducto invProductoIngresar) {
        this.invProductoIngresar = invProductoIngresar;
    }

    public int getId_inv_tallaEditar() {
        return id_inv_tallaEditar;
    }

    public void setId_inv_tallaEditar(int id_inv_tallaEditar) {
        this.id_inv_tallaEditar = id_inv_tallaEditar;
    }

    public double getCantidad_totalIngresar() {
        return cantidad_totalIngresar;
    }

    public void setCantidad_totalIngresar(double cantidad_totalIngresar) {
        this.cantidad_totalIngresar = cantidad_totalIngresar;
    }
	
}
