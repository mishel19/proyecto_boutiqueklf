package boutiqueklf.controller.compras;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import boutiqueklf.controller.JSFUtil;
import boutiqueklf.model.compras.dtos.DTOCompras;
import boutiqueklf.model.compras.managers.ManagerCompras;
import boutiqueklf.model.core.entities.CompProveedor;
import boutiqueklf.model.core.entities.InvProducto;

@Named
@SessionScoped
public class BeanCompras implements Serializable {

	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerCompras mcompras;
	
	private List<InvProducto> listaProductos;
	private List<CompProveedor> listaProveedores;
	private List<DTOCompras> listaCompras;

	private int idProductoSeleccionado;
	private int idProveedorSeleccionado;
	
	private double precio;
	private double total;
	private int cantidad;
	
	public BeanCompras() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void inicializar() {
		listaProductos = mcompras.findAllInvProducto();
		listaProveedores = mcompras.findAllCompProveedores();
	}
	
	public void actionListenerAgregarDetalleCompra() {
		listaCompras.add(mcompras.crearDetalleCompra(idProductoSeleccionado, idProveedorSeleccionado, cantidad));
		JSFUtil.crearMensajeINFO("Compra Generada");
		listaCompras = new ArrayList<DTOCompras>();
		total = 0;
		
	}
	
	public List<InvProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<InvProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<CompProveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<CompProveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public int getIdProductoSeleccionado() {
		return idProductoSeleccionado;
	}

	public void setIdProductoSeleccionado(int idProductoSeleccionado) {
		this.idProductoSeleccionado = idProductoSeleccionado;
	}

	public int getIdProveedorSeleccionado() {
		return idProveedorSeleccionado;
	}

	public void setIdProveedorSeleccionado(int idProveedorSeleccionado) {
		this.idProveedorSeleccionado = idProveedorSeleccionado;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	public List<DTOCompras> getListaCompras() {
		return listaCompras;
	}

	public void setListaCompras(List<DTOCompras> listaCompras) {
		this.listaCompras = listaCompras;
	}

}
