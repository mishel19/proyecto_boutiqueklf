package boutiqueklf.controller.compras.proveedores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import boutiqueklf.controller.JSFUtil;
import boutiqueklf.controller.compras.BeanCompras;
import boutiqueklf.model.core.entities.CompProveedor;
import boutiqueklf.model.core.entities.SegUsuario;
import boutiqueklf.model.core.managers.ManagerDAO;
import boutiqueklf.model.proveedores.managers.ManagerProveedor;

@Named
@SessionScoped
public class BeanProveedor implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerProveedor mproveedores;
	
	private List<CompProveedor> listaProveedores;
	private String seleccionar_ruc;
	private CompProveedor nuevoProveedor;
	private CompProveedor edicionProveedor;
	
	@PostConstruct
	public void inicializar( ) {
		nuevoProveedor = new CompProveedor();
	}
	
	public BeanProveedor() {
		// TODO Auto-generated constructor stub
	}
	
	public String actionCargarMenuProveedor() {
		listaProveedores = mproveedores.findAllProveedor();
		return "proveedor?faces-redirect=true";
	}
	
	public String actionCargarMenuProveedoresActualizar() {
		listaProveedores=mproveedores.findAllProveedor();
		return "proveedor_edicion?faces-redirect=true";
	}
	
	public String actionCargarMenuProveedorEliminar() {
		listaProveedores=mproveedores.findAllProveedor();
		return "proveedor_eliminar?faces-redirect=true";
	}
	
	public void actionListenerSeleccionarProveedorEdicion(CompProveedor p) {
		edicionProveedor = p;
		seleccionar_ruc = p.getRucCompProveedor();
	}
	
	public void actionListenerCargaProveedores(CompProveedor pr) {
		edicionProveedor = pr;
	}
	
	public void actionListenerInsertarNuevoProveedor() {
		try {
			mproveedores.insertarProveedor(nuevoProveedor);
			listaProveedores=mproveedores.findAllProveedor();
			nuevoProveedor=new CompProveedor();
			JSFUtil.crearMensajeINFO("Proveedor insertado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
		
	public void actionListenerActualizarEdicionProveedor() {
		try {
			mproveedores.actualizarProveedor(edicionProveedor);
			JSFUtil.crearMensajeINFO("Proveedor actualizado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void actionListenerEliminarProveedor(String rucProveedor) {
		try {
			mproveedores.eliminarProveedor(rucProveedor);
			listaProveedores=mproveedores.findAllProveedor();
			JSFUtil.crearMensajeINFO("Proveedor eliminado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	
	public List<CompProveedor> getListaProveedores() {
		return listaProveedores;
	}

	public void setListaProveedores(List<CompProveedor> listaProveedores) {
		this.listaProveedores = listaProveedores;
	}

	public CompProveedor getNuevoProveedor() {
		return nuevoProveedor;
	}

	public void setNuevoProveedor(CompProveedor nuevoProveedor) {
		this.nuevoProveedor = nuevoProveedor;
	}

	public CompProveedor getEdicionProveedor() {
		return edicionProveedor;
	}

	public void setEdicionProveedor(CompProveedor edicionProveedor) {
		this.edicionProveedor = edicionProveedor;
	}

	public String getSeleccionar_ruc() {
		return seleccionar_ruc;
	}

	public void setSeleccionar_ruc(String seleccionar_ruc) {
		this.seleccionar_ruc = seleccionar_ruc;
	}
	
	
}
