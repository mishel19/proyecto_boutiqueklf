package boutiqueklf.model.proveedores.dtos;

import java.awt.image.DirectColorModel;
import java.io.Serializable;

import boutiqueklf.model.core.entities.CompProveedor;

public class DTOProveeedor implements Serializable {
	private String ruc_comp_proveedor; 
	private String  nombre_prove; 
	private String  apellido_prove; 
	private String  direccion_prove; 
	private String telefono_prove; 
	private String  correo_prive;
	
	public DTOProveeedor(String ruc_comp_proveedor, String nombre_prove, String apellido_prove, String direccion_prove,
			String telefono_prove, String correo_prive) {
		super();
		this.ruc_comp_proveedor = ruc_comp_proveedor;
		this.nombre_prove = nombre_prove;
		this.apellido_prove = apellido_prove;
		this.direccion_prove = direccion_prove;
		this.telefono_prove = telefono_prove;
		this.correo_prive = correo_prive;
	}
	
	public String getRuc_comp_proveedor() {
		return ruc_comp_proveedor;
	}
	public void setRuc_comp_proveedor(String ruc_comp_proveedor) {
		this.ruc_comp_proveedor = ruc_comp_proveedor;
	}
	public String getNombre_prove() {
		return nombre_prove;
	}
	public void setNombre_prove(String nombre_prove) {
		this.nombre_prove = nombre_prove;
	}
	public String getApellido_prove() {
		return apellido_prove;
	}
	public void setApellido_prove(String apellido_prove) {
		this.apellido_prove = apellido_prove;
	}
	public String getDireccion_prove() {
		return direccion_prove;
	}
	public void setDireccion_prove(String direccion_prove) {
		this.direccion_prove = direccion_prove;
	}
	public String getTelefono_prove() {
		return telefono_prove;
	}
	public void setTelefono_prove(String telefono_prove) {
		this.telefono_prove = telefono_prove;
	}
	public String getCorreo_prive() {
		return correo_prive;
	}
	public void setCorreo_prive(String correo_prive) {
		this.correo_prive = correo_prive;
	} 
	
	
}
