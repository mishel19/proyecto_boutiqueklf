package boutiqueklf.model.proveedores.managers;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import boutiqueklf.model.auditoria.managers.ManagerAuditoria;
import boutiqueklf.model.core.entities.CompProveedor;
import boutiqueklf.model.core.entities.SegModulo;
import boutiqueklf.model.core.entities.SegUsuario;
import boutiqueklf.model.core.managers.ManagerDAO;
import boutiqueklf.model.proveedores.dtos.DTOProveeedor;
import boutiqueklf.model.seguridades.dtos.LoginDTO;

/**
 * Session Bean implementation class ManagerProveedor
 */
@Stateless
@LocalBean
public class ManagerProveedor {
	@EJB
	private ManagerDAO mDAO;
	@EJB
	private ManagerAuditoria mAuditoria;
	
	@PersistenceContext
	private EntityManager emp;

    /**
     * Default constructor. 
     */
    public ManagerProveedor() {
        // TODO Auto-generated constructor stub
    }
    
	public List<CompProveedor> findAllProveedor(){
		//TypedQuery<CompProveedor q = emp.createNamedQuery("CompProveedor.findAll", CompProveedor.class);
    	//return q.getResultList();
    	return emp.createNamedQuery("CompProveedor.findAll", CompProveedor.class).getResultList();
    }
    
    public void insertarProveedor(CompProveedor nuevoProveedor) throws Exception {
    	CompProveedor prov = new CompProveedor();
    	prov.setRucCompProveedor(nuevoProveedor.getRucCompProveedor());
    	prov.setNombreProve(nuevoProveedor.getNombreProve());
		prov.setApellidoProve(nuevoProveedor.getApellidoProve());
		prov.setDireccionProve(nuevoProveedor.getDireccionProve());
		prov.setTelefonoProve(nuevoProveedor.getTelefonoProve());
		prov.setTelefonoProve(nuevoProveedor.getCorreoPrive());
    	//mDAO.insertar(nuevoProveedor);
		emp.persist(prov);
    }
    
    public void actualizarProveedor(CompProveedor edicionProveedor) throws Exception {
    	//CompProveedor proveedor=(CompProveedor) mDAO.findById(CompProveedor.class, edicionProveedor.getRucCompProveedor());
    	CompProveedor proveedor = emp.find(CompProveedor.class, edicionProveedor.getRucCompProveedor());
    	proveedor.setNombreProve(edicionProveedor.getNombreProve());
    	proveedor.setApellidoProve(edicionProveedor.getApellidoProve());
    	proveedor.setDireccionProve(edicionProveedor.getDireccionProve());
    	proveedor.setTelefonoProve(edicionProveedor.getTelefonoProve());
    	proveedor.setCorreoPrive(edicionProveedor.getCorreoPrive());
    	emp.merge(proveedor);
    	//mDAO.actualizar(proveedor);
    	//mAuditoria.mostrarLog(getClass(), "actualizarProvedor", "se actualizó al proveedor "+proveedor.getApellidoProve());
    }
    
    public Object findById(Class clase, Object pID) throws Exception {
		if (pID == null)
			throw new Exception("Debe especificar el codigo para buscar el dato.");
		Object o;
		try {
			o = emp.find(clase, pID);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al buscar la informacion especificada (" + pID + ") : " + e.getMessage());
		}
		return o;
	}
    
    public void eliminar(Class clase, Object pID) throws Exception {
		if (pID == null) {
			throw new Exception("Debe especificar un identificador para eliminar el dato solicitado.");
		}
		Object o = findById(clase, pID);
		try {
			emp.remove(o);
		} catch (Exception e) {
			throw new Exception("No se pudo eliminar el dato: " + e.getMessage());
		}
	}

    public void eliminarProveedor(String rucProveedor) throws Exception {
    	//mDAO.eliminar(CompProveedor.class, IdProveedor);
    	eliminar(CompProveedor.class, rucProveedor);
    }
}
