package boutiqueklf.model.inventario.managers;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import boutiqueklf.model.core.entities.CompProveedorDetalle;
import boutiqueklf.model.core.entities.InvProducto;
import boutiqueklf.model.core.entities.InvTalla;


/**
 * Session Bean implementation class ManagerInvProducto
 */
@Stateless
@LocalBean
public class ManagerInvProducto {
	@PersistenceContext
    private EntityManager em;
    //private ManagerInvProducto mp;

    /**
     * Default constructor.
     */
    public ManagerInvProducto() {
        // TODO Auto-generated constructor stub
    }

    public List<InvProducto> findAllInvProducto() {
        return em.createQuery("SELECT p FROM InvProducto p ORDER BY p.idProd ASC", InvProducto.class).getResultList();

    }

    public List<CompProveedorDetalle> findAllCompProveedorDetalle() {

        return em.createNamedQuery("CompProveedorDetalle.findAll", CompProveedorDetalle.class).getResultList();
    }

    public List<InvTalla> findAllInvTalla() {

        return em.createNamedQuery("InvTalla.findAll", InvTalla.class).getResultList();
    }
    // public List<ThmEmpleado> findAllThmEmpleado(){
    //TypedQuery<ThmEmpleado> q = em.createQuery("select m from ThmEmpleado m", ThmEmpleado.class);
    //return q.getResultList();    
    //}

    public void crearInvProducto(int id_inv_talla, String nombre_producto, String descripcion, double precio, double cantidad_total) {
        InvTalla talla = em.find(InvTalla.class, id_inv_talla);
        InvProducto inventario = new InvProducto();
        //inventario.setIdProd(id_prod);
        inventario.setInvTalla(talla);
        inventario.setNombreProducto(nombre_producto);
        inventario.setDescripcion(descripcion);
        inventario.setPrecio(new BigDecimal(precio));
        inventario.setCantidadTotal(new BigDecimal(cantidad_total));
        em.persist(inventario);

    }

    public void ingresarInvProducto(InvProducto imInvProductoIngresar,double cantidad) {
        //InvTalla talla = em.find(InvTalla.class, invProductoIngresar.getInvTalla().getIdInvTalla());
        InvProducto producto = em.find(InvProducto.class, imInvProductoIngresar.getIdProd());
        //producto.setInvTalla(talla);
        //producto.setNombreProducto(invProductoIngresar.getNombreProducto());
        //producto.setDescripcion(invProductoIngresar.getDescripcion());
        //producto.setPrecio(new BigDecimal(invProductoIngresar.getPrecio().doubleValue()));
        producto.setCantidadTotal(new BigDecimal(imInvProductoIngresar.getCantidadTotal().doubleValue()+cantidad));
        em.merge(producto);

    }

    public void actualizarInvProducto(InvProducto imInvProductoEditar, int id_talla) {
        
        InvProducto producto = em.find(InvProducto.class, imInvProductoEditar.getIdProd());
        InvTalla talla = em.find(InvTalla.class, id_talla);
//        JSFUtil.crearMensajeWarning(""+talla.getNombreTalla());
        producto.setInvTalla(talla);
        producto.setNombreProducto(imInvProductoEditar.getNombreProducto());
        producto.setDescripcion(imInvProductoEditar.getDescripcion());
        producto.setPrecio(new BigDecimal(imInvProductoEditar.getPrecio().doubleValue()));
        producto.setCantidadTotal(new BigDecimal(imInvProductoEditar.getCantidadTotal().doubleValue()));
        em.merge(producto);

    }

    public boolean eliminarInvProducto(int id_prod) {
        boolean a = false;
        InvProducto producto = em.find(InvProducto.class, id_prod);
        if (producto != null) {
            em.remove(producto);
            a = true;

        }
        return a;
    }

    public InvProducto findProductoByid(int id_prod) {
        InvProducto producto = em.find(InvProducto.class, id_prod);
        return producto;
    }

}





