package boutiqueklf.model.compras.managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import boutiqueklf.model.compras.dtos.DTOCompras;
import boutiqueklf.model.core.entities.CompProveedor;
import boutiqueklf.model.core.entities.InvProducto;

/**
 * Session Bean implementation class ManagerCompras
 */
@Stateless
@LocalBean
public class ManagerCompras {

	@PersistenceContext
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public ManagerCompras() {
        // TODO Auto-generated constructor stub
    }
    
    public List<InvProducto> findAllInvProducto(){
    	TypedQuery<InvProducto> q = em.createQuery("select m from InvProducto m", InvProducto.class);
    	return q.getResultList();    
    }
    
    public List<CompProveedor> findAllCompProveedores(){
    	TypedQuery<CompProveedor> q = em.createQuery("select m from CompProveedor m", CompProveedor.class);
    	return q.getResultList();    
    }
    
    public DTOCompras crearDetalleCompra(int idproducto,int idproveedor, int cantidad) {
    	InvProducto a = em.find(InvProducto.class, idproducto);
    	CompProveedor c = em.find(CompProveedor.class, idproveedor);
    	
    	DTOCompras detalle = new DTOCompras();
    	detalle.setId_producto(idproducto);
    	detalle.setNombre(a.getNombreProducto());
    	detalle.setCantidad_comp(cantidad);
    	detalle.setRuc_comp_proveedor(idproveedor);
    	//detalle.setFecha();
    	detalle.setPrecio_comp(a.getPrecio().doubleValue());
    	detalle.setTotal_comp(a.getPrecio().doubleValue()*cantidad);
    	
    	return detalle;
    }

}
