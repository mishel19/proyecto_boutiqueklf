package boutiqueklf.model.compras.dtos;

import java.util.Date;

public class DTOCompras {
	private int  id_comp_proveedor_detalle; 
	private int  id_producto; 
	private int  ruc_comp_proveedor; 
	private Date fecha; 
	private double precio_comp;
	private String nombre;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	private int cantidad_comp; 
	private double total_comp;
	public int getId_comp_proveedor_detalle() {
		return id_comp_proveedor_detalle;
	}
	public void setId_comp_proveedor_detalle(int id_comp_proveedor_detalle) {
		this.id_comp_proveedor_detalle = id_comp_proveedor_detalle;
	}
	public int getId_producto() {
		return id_producto;
	}
	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}
	public int getRuc_comp_proveedor() {
		return ruc_comp_proveedor;
	}
	public void setRuc_comp_proveedor(int ruc_comp_proveedor) {
		this.ruc_comp_proveedor = ruc_comp_proveedor;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public double getPrecio_comp() {
		return precio_comp;
	}
	public void setPrecio_comp(double precio_comp) {
		this.precio_comp = precio_comp;
	}
	public int getCantidad_comp() {
		return cantidad_comp;
	}
	public void setCantidad_comp(int cantidad_comp) {
		this.cantidad_comp = cantidad_comp;
	}
	public double getTotal_comp() {
		return total_comp;
	}
	public void setTotal_comp(double total_comp) {
		this.total_comp = total_comp;
	}

	
}
