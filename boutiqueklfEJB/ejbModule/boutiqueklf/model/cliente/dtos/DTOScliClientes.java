package boutiqueklf.model.cliente.dtos;

import java.awt.image.DirectColorModel;
import java.io.Serializable;

import boutiqueklf.model.core.entities.CliCliente;

public class DTOScliClientes implements Serializable {

	private String cedula_cliente;
	private String nombre_cliente;
	private String apellido_cliente;

	private String telefono;
	private  String correo;
	private String nombreCiudad;
	private String provincia;
	
	
	
	
	
	public DTOScliClientes(String cedula_cliente, String nombre_cliente, String apellido_cliente, String telefono,
			String correo, String nombreCiudad, String provincia) {
		super();
		this.cedula_cliente = cedula_cliente;
		this.nombre_cliente = nombre_cliente;
		this.apellido_cliente = apellido_cliente;
		this.telefono = telefono;
		this.correo = correo;
		this.nombreCiudad = nombreCiudad;
		this.provincia = provincia;
	}
	public String getCedula_cliente() {
		return cedula_cliente;
	}
	public void setCedula_cliente(String cedula_cliente) {
		this.cedula_cliente = cedula_cliente;
	}
	public String getNombre_cliente() {
		return nombre_cliente;
	}
	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}
	public String getApellido_cliente() {
		return apellido_cliente;
	}
	public void setApellido_cliente(String apellido_cliente) {
		this.apellido_cliente = apellido_cliente;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getNombreCiudad() {
		return nombreCiudad;
	}
	public void setNombreCiudad(String nombreCiudad) {
		this.nombreCiudad = nombreCiudad;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	
	
	
	
	
}