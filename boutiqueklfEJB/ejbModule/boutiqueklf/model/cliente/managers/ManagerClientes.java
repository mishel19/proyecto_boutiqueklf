package boutiqueklf.model.cliente.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import boutiqueklf.model.core.entities.CliCliente;
import boutiqueklf.model.core.entities.SegModulo;
import boutiqueklf.model.core.entities.SegUsuario;
import boutiqueklf.model.core.entities.ThmCargo;
import boutiqueklf.model.core.entities.ThmEmpleado;
import boutiqueklf.model.core.managers.ManagerDAO;
import boutiqueklf.model.seguridades.managers.ManagerSeguridades;
import boutiqueklf.model.thumano.dtos.DTOThmCargo;

/**
 * Session Bean implementation class ManagerClientes
 */
@Stateless
//@LocalBean
public class ManagerClientes {
	


	private ManagerDAO mDAO;
	
	@PersistenceContext
	private EntityManager em2;
	
	
    public ManagerClientes() {
    }
    
    public List<CliCliente> findAllCliClientesv5(){
    	//TypedQuery<CliCliente> q = em.createNamedQuery("CliCliente.findAll", CliCliente.class);
    	//return q.getResultList();   
    return em2.createNamedQuery("CliCliente.findAll", CliCliente.class).getResultList();

    	
    }
  
    //public List<CliCliente> findAllCliClientesv5(){
    	//return mDAO.findAll(CliCliente.class);
    	//return.em.createNamedQuery("CliCliente.findAll",CliCliente.class).getResultList();
    //}
    
   
    //actualizar--direcciones ****
    public void actualizarClientev5(CliCliente ac_cliente) throws Exception {
    	//CliCliente cli=(CliCliente)mDAO.findById(CliCliente.class, ac_cliente.getCedulaCli());
    	CliCliente cli=em2.find(CliCliente.class, ac_cliente.getCedulaCli());	
    	cli.setNombreCli(ac_cliente.getNombreCli());
    	cli.setApellidoCli(ac_cliente.getApellidoCli());
    	cli.setTelefonoCli(ac_cliente.getTelefonoCli());
    	cli.setCorreoCli(ac_cliente.getCorreoCli());
    	cli.setNombreCiudadCli(ac_cliente.getNombreCiudadCli());
    	cli.setProvinciaCli(ac_cliente.getProvinciaCli());
		em2.merge(cli);
    	//mDAO.actualizar(cli);
    }
	public Object findById(Class clase, Object pID) throws Exception {
		if (pID == null)
			throw new Exception("Debe especificar el codigo para buscar el dato.");
		Object o;
		try {
			o = em2.find(clase, pID);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error al buscar la informacion especificada (" + pID + ") : " + e.getMessage());
		}
		return o;
	}
    
    public void eliminar(Class clase, Object pID) throws Exception {
		if (pID == null) {
			throw new Exception("Debe especificar un identificador para eliminar el dato solicitado.");
		}
		Object o = findById(clase, pID);
		try {
			em2.remove(o);
		} catch (Exception e) {
			throw new Exception("No se pudo eliminar el dato: " + e.getMessage());
		}
	}
    //elimniar cliente v5 ***
    public void eliminarClientev5(String cedula_seleccionada) throws Exception {
     	//mDAO.eliminar(CliCliente.class, cedula_seleccionada);
    	//em2.remove(CliCliente.class, cedula_seleccionada);
    	eliminar(CliCliente.class, cedula_seleccionada);
    }
    

    
    //insertarCLiente v5  ***
    public void insertarClientev5(CliCliente nuevo_cliente) throws Exception {
    	CliCliente cliC=new CliCliente();
    	//CliCliente cliente=em.find(CliCliente.class, nuevo_cliente);
    	cliC.setCedulaCli(nuevo_cliente.getCedulaCli());
    	cliC.setNombreCli(nuevo_cliente.getNombreCli());
    	cliC.setApellidoCli(nuevo_cliente.getApellidoCli());
    	cliC.setTelefonoCli(nuevo_cliente.getTelefonoCli());
    	cliC.setCorreoCli(nuevo_cliente.getCorreoCli());
    	cliC.setNombreCiudadCli(nuevo_cliente.getNombreCiudadCli());
    	cliC.setProvinciaCli(nuevo_cliente.getProvinciaCli());
    	//insertarv5(cliC);
        //mDAO.insertar(cliC);
    	em2.persist(cliC);
    	//em.persist(cliC);
    	//return cliC;
    	
    }
 
  
  
  
    

    
 
 

}
