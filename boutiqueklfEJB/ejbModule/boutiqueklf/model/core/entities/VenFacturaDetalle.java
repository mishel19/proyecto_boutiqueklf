package boutiqueklf.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ven_factura_detalle database table.
 * 
 */
@Entity
@Table(name="ven_factura_detalle")
@NamedQuery(name="VenFacturaDetalle.findAll", query="SELECT v FROM VenFacturaDetalle v")
public class VenFacturaDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ven_factura_detalle", unique=true, nullable=false)
	private Integer idVenFacturaDetalle;

	@Column(nullable=false, precision=131089)
	private BigDecimal cantidad;

	@Column(nullable=false, precision=8, scale=2)
	private BigDecimal total;

	//bi-directional many-to-one association to InvProducto
	@ManyToOne
	@JoinColumn(name="id_prod")
	private InvProducto invProducto;

	//bi-directional many-to-one association to VenFacturaCabecera
	@ManyToOne
	@JoinColumn(name="id_ven_factura_cabecera")
	private VenFacturaCabecera venFacturaCabecera;

	public VenFacturaDetalle() {
	}

	public Integer getIdVenFacturaDetalle() {
		return this.idVenFacturaDetalle;
	}

	public void setIdVenFacturaDetalle(Integer idVenFacturaDetalle) {
		this.idVenFacturaDetalle = idVenFacturaDetalle;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public InvProducto getInvProducto() {
		return this.invProducto;
	}

	public void setInvProducto(InvProducto invProducto) {
		this.invProducto = invProducto;
	}

	public VenFacturaCabecera getVenFacturaCabecera() {
		return this.venFacturaCabecera;
	}

	public void setVenFacturaCabecera(VenFacturaCabecera venFacturaCabecera) {
		this.venFacturaCabecera = venFacturaCabecera;
	}

}