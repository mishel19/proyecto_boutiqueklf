package boutiqueklf.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cli_cliente database table.
 * 
 */
@Entity
@Table(name="cli_cliente")
@NamedQuery(name="CliCliente.findAll", query="SELECT c FROM CliCliente c")
public class CliCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cedula_cli", unique=true, nullable=false, length=10)
	private String cedulaCli;

	@Column(name="apellido_cli", nullable=false, length=50)
	private String apellidoCli;

	@Column(name="correo_cli", nullable=false, length=50)
	private String correoCli;

	@Column(name="nombre_ciudad_cli", nullable=false, length=50)
	private String nombreCiudadCli;

	@Column(name="nombre_cli", nullable=false, length=50)
	private String nombreCli;

	@Column(name="provincia_cli", nullable=false, length=50)
	private String provinciaCli;

	@Column(name="telefono_cli", nullable=false, length=50)
	private String telefonoCli;

	//bi-directional many-to-one association to VenFacturaCabecera
	@OneToMany(mappedBy="cliCliente")
	private List<VenFacturaCabecera> venFacturaCabeceras;

	public CliCliente() {
	}

	public String getCedulaCli() {
		return this.cedulaCli;
	}

	public void setCedulaCli(String cedulaCli) {
		this.cedulaCli = cedulaCli;
	}

	public String getApellidoCli() {
		return this.apellidoCli;
	}

	public void setApellidoCli(String apellidoCli) {
		this.apellidoCli = apellidoCli;
	}

	public String getCorreoCli() {
		return this.correoCli;
	}

	public void setCorreoCli(String correoCli) {
		this.correoCli = correoCli;
	}

	public String getNombreCiudadCli() {
		return this.nombreCiudadCli;
	}

	public void setNombreCiudadCli(String nombreCiudadCli) {
		this.nombreCiudadCli = nombreCiudadCli;
	}

	public String getNombreCli() {
		return this.nombreCli;
	}

	public void setNombreCli(String nombreCli) {
		this.nombreCli = nombreCli;
	}

	public String getProvinciaCli() {
		return this.provinciaCli;
	}

	public void setProvinciaCli(String provinciaCli) {
		this.provinciaCli = provinciaCli;
	}

	public String getTelefonoCli() {
		return this.telefonoCli;
	}

	public void setTelefonoCli(String telefonoCli) {
		this.telefonoCli = telefonoCli;
	}

	public List<VenFacturaCabecera> getVenFacturaCabeceras() {
		return this.venFacturaCabeceras;
	}

	public void setVenFacturaCabeceras(List<VenFacturaCabecera> venFacturaCabeceras) {
		this.venFacturaCabeceras = venFacturaCabeceras;
	}

	public VenFacturaCabecera addVenFacturaCabecera(VenFacturaCabecera venFacturaCabecera) {
		getVenFacturaCabeceras().add(venFacturaCabecera);
		venFacturaCabecera.setCliCliente(this);

		return venFacturaCabecera;
	}

	public VenFacturaCabecera removeVenFacturaCabecera(VenFacturaCabecera venFacturaCabecera) {
		getVenFacturaCabeceras().remove(venFacturaCabecera);
		venFacturaCabecera.setCliCliente(null);

		return venFacturaCabecera;
	}

}