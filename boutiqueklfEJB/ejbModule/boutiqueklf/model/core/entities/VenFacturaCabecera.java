package boutiqueklf.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ven_factura_cabecera database table.
 * 
 */
@Entity
@Table(name="ven_factura_cabecera")
@NamedQuery(name="VenFacturaCabecera.findAll", query="SELECT v FROM VenFacturaCabecera v")
public class VenFacturaCabecera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_ven_factura_cabecera", unique=true, nullable=false)
	private Integer idVenFacturaCabecera;

	@Column(nullable=false, precision=8, scale=2)
	private BigDecimal descuento;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date fecha;

	@Column(nullable=false, precision=8, scale=2)
	private BigDecimal iva;

	@Column(nullable=false, precision=8, scale=2)
	private BigDecimal subtotal;

	@Column(nullable=false, precision=8, scale=2)
	private BigDecimal total;

	//bi-directional many-to-one association to CliCliente
	@ManyToOne
	@JoinColumn(name="cedula_cliente")
	private CliCliente cliCliente;

	//bi-directional many-to-one association to ThmEmpleado
	@ManyToOne
	@JoinColumn(name="id_thm_empleado")
	private ThmEmpleado thmEmpleado;

	//bi-directional many-to-one association to VenFacturaDetalle
	@OneToMany(mappedBy="venFacturaCabecera",cascade = CascadeType.ALL)
	private List<VenFacturaDetalle> venFacturaDetalles;

	public VenFacturaCabecera() {
	}

	public Integer getIdVenFacturaCabecera() {
		return this.idVenFacturaCabecera;
	}

	public void setIdVenFacturaCabecera(Integer idVenFacturaCabecera) {
		this.idVenFacturaCabecera = idVenFacturaCabecera;
	}

	public BigDecimal getDescuento() {
		return this.descuento;
	}

	public void setDescuento(BigDecimal descuento) {
		this.descuento = descuento;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public CliCliente getCliCliente() {
		return this.cliCliente;
	}

	public void setCliCliente(CliCliente cliCliente) {
		this.cliCliente = cliCliente;
	}

	public ThmEmpleado getThmEmpleado() {
		return this.thmEmpleado;
	}

	public void setThmEmpleado(ThmEmpleado thmEmpleado) {
		this.thmEmpleado = thmEmpleado;
	}

	public List<VenFacturaDetalle> getVenFacturaDetalles() {
		return this.venFacturaDetalles;
	}

	public void setVenFacturaDetalles(List<VenFacturaDetalle> venFacturaDetalles) {
		this.venFacturaDetalles = venFacturaDetalles;
	}

	public VenFacturaDetalle addVenFacturaDetalle(VenFacturaDetalle venFacturaDetalle) {
		getVenFacturaDetalles().add(venFacturaDetalle);
		venFacturaDetalle.setVenFacturaCabecera(this);

		return venFacturaDetalle;
	}

	public VenFacturaDetalle removeVenFacturaDetalle(VenFacturaDetalle venFacturaDetalle) {
		getVenFacturaDetalles().remove(venFacturaDetalle);
		venFacturaDetalle.setVenFacturaCabecera(null);

		return venFacturaDetalle;
	}

}